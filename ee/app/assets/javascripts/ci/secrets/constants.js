export const INDEX_ROUTE_NAME = 'index';
export const NEW_ROUTE_NAME = 'new';
export const DETAILS_ROUTE_NAME = 'details';
export const AUDIT_LOG_ROUTE_NAME = 'auditlog';
export const EDIT_ROUTE_NAME = 'edit';

// mock data so we can navigate around
export const mockGroupSecretsData = [{ key: 'group_secret_1' }, { key: 'group_secret_2' }];
export const mockProjectSecretsData = [{ key: 'project_secret_1' }, { key: 'project_secret_2' }];
